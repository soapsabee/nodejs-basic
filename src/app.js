const path = require('path');
const ejs = require('ejs');
const express = require('express');
const morgan = require('morgan');
const todos = require('./routes/todos');
const app = express();
const port = process.env.PORT || 3000;
// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended:true }))

// parse application/json
app.use(express.json());



//logger
if (app.get('env') === 'development'){
    app.use(morgan('dev'));
}
    app.use(express.static('public'));
// view engine setup
app.set('views', path.join(__dirname,'views'));
app.set('view engine','ejs');

//default route
app.get('/',(req,res)=>{
    res.render('index',{name: "Patiphan"});
})



app.use('/todos', todos);

app.post('/message', function(req,res){
    console.log(req.body);
    res.send('Got a POST request');
})



app.get('/', (req,res)=>{
    res.send('Hello World!');
});

app.post('/', function(req,res){
    res.send('Got a POST request');
});

app.put('/user',function(req,res){

    res.send('Got a PUT request at /user');

});

app.delete('/user', function(req,res){
    res.send('Got a DELETE request at /user');
});

app.get('/greeting/:name', (req, res) => {

    console.log(req.params);
    const {name} = req.params;
    res.send(`Hello ${name}`);

});

app.get('/greeting',(req,res)=>{
    console.log(req.query);
    const {name} = req.query;
    res.send(`Hello ${name}`);
})

app.listen(port, ()=> {
    console.log(`Server listening on port ${port}!`);
});